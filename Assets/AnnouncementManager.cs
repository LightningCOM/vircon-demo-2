﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using TMPro;
using UnityEngine.UI;

public class AnnouncementManager : MonoBehaviourPunCallbacks, IMatchmakingCallbacks
{

    public GameObject textGameObject;
    public Transform panelParent;
    public GameObject controlPanel;
    public GameObject passwordPanel;
    public GameObject incorrectPassword;

    public string Password;

    private string textAnnouncement; 
    private int notifDuration;

    public InputField announcementInputField;
    public InputField durationInputField;

    public TMP_InputField passwordInputField;

    bool passwordPass;


    private void Start()
    {
        passwordPass = false;
    }
    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F9))
        {
            if (!passwordPass)
            {
                incorrectPassword.active = false;
                passwordPanel.active = !passwordPanel.active;
            }
            else
            {
                controlPanel.active = !controlPanel.active;
            }
        }
    }

    public void PasswordCheck()
    {
        if(passwordInputField.text == Password)
        {
            controlPanel.active = true;
            passwordPanel.active = false;
            passwordPass = true;
        }
        else
        {
            incorrectPassword.active = true;
        }
    }


    [PunRPC]
    void CreateAnnouncement(string text, int duration)
    {
        //  Debug.Log(string.Format("Timer: {0} ", a));
        // Instantiate the prefab
        // get the prefab text
        GameObject newOne = Instantiate(textGameObject, Vector3.zero, Quaternion.identity);
        newOne.transform.SetParent(panelParent);
        newOne.SetActive(true);
        newOne.transform.localScale = Vector3.one;
        TMP_Text messageTMP = newOne.GetComponentInChildren<TMP_Text>();
        messageTMP.text = text;
        Destroy(newOne, duration);
    }


    public void StartAnnouncement()
    {
        if(announcementInputField.text == "" || durationInputField.text == "")
        {
            Debug.LogError("Can't send an announcement, please input field!");
            textAnnouncement = null;
            notifDuration = 0;
            return; 
        }

        textAnnouncement = announcementInputField.text;
        notifDuration = int.Parse(durationInputField.text);
        Debug.LogFormat("Create Announcement: {0} / Duration: {1} " , textAnnouncement , notifDuration);
        this.photonView.RPC("CreateAnnouncement", RpcTarget.All, textAnnouncement, notifDuration);
        controlPanel.SetActive(false);
        textAnnouncement = null;
        notifDuration = 0;
    }


    public override void OnJoinedRoom()
    {
        Debug.Log("On joined to room Announcement Manager " + SceneManagerHelper.ActiveSceneName);
    }


}
