﻿using SpaceHub.Conference;
using UnityEngine;

public class BoothSpawnPoint : SpawnPoint
{
    private void Awake()
    {
        var id = transform.parent.parent.name;
        try
        {
            GetComponentInParent<LoadSceneInteractable>().ReturnSpawnPointId = id;
            Id = id;
        }
        catch (System.Exception)
        {

            Debug.LogWarning(id);
        }
        
    }
}
