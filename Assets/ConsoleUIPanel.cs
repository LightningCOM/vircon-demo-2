﻿using UnityEngine;

public class ConsoleUIPanel : MonoBehaviour
{
    private GameObject _overallBackButton;
    private void Awake()
    {
        _overallBackButton = FindObjectOfType<OverAllBackButton>().gameObject;
    }
    private void OnEnable()
    {
        _overallBackButton.SetActive(false);
    }

    private void OnDisable()
    {
        _overallBackButton.SetActive(true);
    }
}
