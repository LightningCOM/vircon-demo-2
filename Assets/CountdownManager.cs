﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using SpaceHub.Conference;

public class CountdownManager : MonoBehaviourPunCallbacks, IMatchmakingCallbacks
{
    private GameConsole[] GameConsoles; 
    private string timeText = "";
    private bool isTimerFinished = true;
    private bool isTimerStarted = false; 
    private bool isTimerPause = false;
    private int totalSeconds = 0;
    private int holderSeconds = 0;

    private bool isIsland2Loaded;


    public TMP_Text TimerControlText;
    public TMP_Text YouAreTheHostText;
    public int startSeconds = 180; // three minutes

    private void Start()
    {
        GameConsoles = FindObjectsOfType<GameConsole>();
    }

    public void SendPlayerBubbleUpdate()
    {
        StartCoroutine(CheckForPlayerBubble());
    }
    private IEnumerator CheckForPlayerBubble()
    {
            yield return new WaitForSeconds(0.5f);
            this.photonView.RPC("UpdatePlayerBubble", RpcTarget.AllBuffered, GameConsoles[0].PlayerBubble.activeSelf, GameConsoles[1].PlayerBubble.activeSelf);
    }

    IEnumerator Countdown(int seconds)
    {

        isTimerFinished = false;
        totalSeconds = seconds;
        
        while (totalSeconds > 0)
        {
            yield return new WaitForSeconds(1);
            totalSeconds--;
            int seconds2 = totalSeconds % 60;
            int minutes = totalSeconds / 60;
            timeText = minutes.ToString("00") + ":" + seconds2.ToString("00");
            //Debug.Log("Countdown: " + timeText);
            if (totalSeconds % 5 == 0)
            {
                this.photonView.RPC("UpdateGameConsoles", RpcTarget.AllBuffered, GameConsoles[0].gameObject.activeSelf, GameConsoles[1].gameObject.activeSelf);
            }
            this.photonView.RPC("TimerMessage", RpcTarget.AllBuffered, timeText, totalSeconds);
           
            //SendTimerUpdate(totalSeconds);
        }
        Destroy(FindObjectOfType<FollowNavmeshAgent>());
        // DoStuff ();  
        //  Debug.Log("Is Timer Finished: " + isTimerFinished);
    }


    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }



    public void OnDestroy()
    {
        if (this.photonView.IsMine)
        {
            if (PhotonNetwork.PlayerListOthers.Length > 0)
            {
                this.photonView.TransferOwnership(PhotonNetwork.PlayerListOthers[0].ActorNumber);
                Debug.Log("Change Ownership to Actor Number: " + PhotonNetwork.PlayerListOthers[0].ActorNumber);
            }

            StopAllCoroutines();
            PhotonNetwork.Disconnect();
            
        }
       
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        Debug.Log("New Master Client: " + newMasterClient.ActorNumber); 
        if (this.photonView.IsMine)
        {
            StartCoroutine(Countdown(holderSeconds));
            YouAreTheHostText.gameObject.SetActive(false);
        }
        else
        {
            YouAreTheHostText.gameObject.SetActive(false);
        }

    }

    [PunRPC]
    void TimerMessage(string a, int seconds)
    {
        //  Debug.Log(string.Format("Timer: {0} ", a));
        TimerControlText.text = a;
        holderSeconds = seconds;
    }

    [PunRPC]
    void UpdateGameConsoles(bool a, bool b)
    {
        var g1 = GameConsoles[1];
        var g2 = GameConsoles[0];
        g2.gameObject.SetActive(a);
        g1.gameObject.SetActive(b);
        if (!b && !isIsland2Loaded)
        {
            g1.Door.SetActive(true);
            g1.CorrectPanel.SetActive(true);
            FindObjectOfType<LoadStaticObjects>().EnableIsland2();
            isIsland2Loaded = true;
        }
        if (!a)
        {
            g2.Door.SetActive(true);
        }
    }

    [PunRPC]
    void UpdatePlayerBubble(bool a, bool b)
    {
        
        var g1 = GameConsoles[1];
        var g2 = GameConsoles[0];
        
            g1.OnConsoleIsActiveUpdate(b);
        
            g2.OnConsoleIsActiveUpdate(a);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("On joined to Countdown Manager room " + SceneManagerHelper.ActiveSceneName);

        if (this.photonView.IsMine)
        {
            if (PhotonNetwork.PlayerListOthers.Length > 0)
            {
                this.photonView.TransferOwnership(PhotonNetwork.PlayerListOthers[0].ActorNumber);
                Debug.Log("Transfer Ownership to Actor Number: " + PhotonNetwork.PlayerListOthers[0].ActorNumber);
            }
            else
            {
                Debug.Log("Start Timer Im the host!!");
                StartCoroutine(Countdown(startSeconds));
                YouAreTheHostText.gameObject.SetActive(false);
            }

      
        }
        else
        {
            YouAreTheHostText.gameObject.SetActive(false);
        }


    }



}
