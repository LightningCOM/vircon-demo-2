﻿using UnityEngine;

public class CurrentBoothData : MonoBehaviour
{
    public static CurrentBoothData Instance;
    public string BoothId { get; set; }
    public bool IsVideoReady { get; set; }
    public Sprite[] BoothImages;
    public string BoothLink;
    public string CompanyName;
    public Sprite DefaultBanner;
    //public
    private void Awake()
    {
        Instance = this;
    }
}
