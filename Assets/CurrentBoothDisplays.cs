﻿using SpaceHub.Conference;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CurrentBoothDisplays : MonoBehaviour
{
    //[SerializeField] private List<Image> Displays;
    [SerializeField] private LoadSceneInteractable ExitDoor;
    //[SerializeField] private VideoManager Screen;
    //[SerializeField] private LoadWebsite Monitor;
    [SerializeField] private TextMeshProUGUI CompanyName;
    private const string imageLocation = "https://virtualx.taktylstudios.com/iacademy/cms/boothdisplays/";
    //2/1.jpg
    private void Awake()
    {
        var currentBoothData = CurrentBoothData.Instance;
        var currentBoothId = currentBoothData.BoothId;
        ExitDoor.SpawnPointId = currentBoothId;
        //Monitor.loadWebsiteURL = currentBoothData.BoothLink;
        currentBoothId = currentBoothId.Replace("booth ", "");
        var boothId = Convert.ToInt32(currentBoothId);
        var sprites = currentBoothData.BoothImages;
        CompanyName.text = currentBoothData.CompanyName;
        //for (int i = 0; i < sprites.Length; i++)
        //{
        //    var s = sprites[i];
        //    if (s)
        //        Displays[i].sprite = s;
        //    else
        //        Displays[i].sprite = currentBoothData.DefaultBanner;

        //}

        //string videoUrl;
        //if (currentBoothData.IsVideoReady)
        //    videoUrl = imageLocation + currentBoothId + "/" + "vid.mp4";
        //else
        //    videoUrl = "https://virtualx.taktylstudios.com/iacademy/cms/bigscreen/Of%20Lenses%20and%20Palettes.mp4";
        //Screen.LoadVideoURL(videoUrl);
    }
}
