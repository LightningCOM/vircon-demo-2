﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DisplayFinalImages : MonoBehaviour
{
    [SerializeField] private Sprite[] FinalImages;
    [SerializeField] private GameObject panel;
    [SerializeField] private Image image;
    private void OnEnable()
    {
        StartCoroutine(DisplayImage());
    }

    private IEnumerator DisplayImage()
    {
        yield return new WaitForSeconds(30);
        var final = FinalImages[Random.Range(0, 4)];
        panel.SetActive(true);
        image.sprite = final;
    }
}
