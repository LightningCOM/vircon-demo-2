﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ExpoBoothDisplays : MonoBehaviour
{
    //[SerializeField] private List<Image> Displays;
    [SerializeField] private TextMeshProUGUI BoothName;
    //2/1.jpg
    private void Start()
    {
        var currentBoothData = CurrentBoothData.Instance;
        var currentBoothId = name;
        var boothId = Convert.ToInt32(currentBoothId.Replace("booth ", ""));
        var hallData = HallData.Instance;
        var hallDataIndex = boothId > 20 ? boothId - 21 : boothId - 1;
       // var sprites = hallData.BoothList[hallDataIndex];
        BoothName.text = hallData.BoothNames[hallDataIndex];
        //for (int i = 0; i < sprites.Length; i++)
        //{
        //    var s = sprites[i];
        //    if (s)
        //        Displays[i].sprite = s;
        //    else
        //        Displays[i].sprite = currentBoothData.DefaultBanner;
        //}
    }
}
