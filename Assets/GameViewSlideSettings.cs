﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameViewSlideSettings : MonoBehaviour
{

    public GameViewEncoder gameViewEncoder;
    public GameViewEncoder gameViewEncoderCamera;

    public Slider qualitySlider;
    public Slider fpsSlider;
    public Text qualityText;
    public Text fpsText;

    public void Start()
    {
        slideQuality();
        slideFps();

    }
     public void slideQuality()
    {
        float quality = qualitySlider.value * 100;
        gameViewEncoder.Quality = (int)quality;
        gameViewEncoderCamera.Quality = (int)quality;
        qualityText.text = "Stream Quality %: " + (int)quality;
    }

    public void slideFps()
    {
        float fps = fpsSlider.value * 60;
        gameViewEncoder.StreamFPS = (int)fps;
        gameViewEncoderCamera.StreamFPS = (int)fps;
        fpsText.text = "Stream Fps: " + (int)fps;
    }
}
