﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class chracterController : MonoBehaviour
{

    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.18f;

    Vector3 velocity;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
	float x = Input.GetAxis("Horizontal");
	float z = Input.GetAxis("Vertical");
	
	Vector3 move = transform.right * x + transform.forward * z;
	
	controller.Move(move * speed * Time.deltaTime);	
	
	velocity.y += gravity * Time.deltaTime;

   	controller.Move(velocity * Time.deltaTime);    	

    }

   
}
