﻿using SpaceHub.Conference;
using UnityEngine;

public class Island2Entrance : MonoBehaviour
{
    [SerializeField] private Transform Island2SpawnPoint;
    [SerializeField] private OtherGameConsole OtherGameConsole;
    public void EnterIsland2()
    {
        var player = PlayerLocal.Instance;
        //Destroy(Island1);
        player.GoToAndLook(Island2SpawnPoint, true, true);
        player.ForceSendCurrentPositionAndRotationWithHighAccuracy(0);
        OtherGameConsole.gameObject.SetActive(true);
    }

}
