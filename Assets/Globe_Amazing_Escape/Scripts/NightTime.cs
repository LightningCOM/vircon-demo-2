﻿using UnityEngine;

public class NightTime : MonoBehaviour
{
    [SerializeField] private GameObject[] ObjectsToTurnOff;
    [SerializeField] private GameObject[] ObjectsToTurnOn;
    public void Execute()
    {
        var rend = GetComponent<Renderer>();
        rend.material.SetTextureOffset("_MainTex", new Vector2(1.5f, 0));
        var light = FindObjectOfType<Light>();
        if (light) light.gameObject.SetActive(false);
        foreach (var o in ObjectsToTurnOff)
        {
            o.SetActive(false);
        }
        foreach (var o in ObjectsToTurnOn)
        {
            o.SetActive(true);
        }
    }

}
