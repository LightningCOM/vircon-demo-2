﻿using System.Collections;
using UnityEngine;

public class OtherGameConsole : MonoBehaviour
{
    private NightTime _nightTime;
    private void Awake()
    {
        StartCoroutine(Delay());
    }

    private IEnumerator Delay()
    {
        //cant find object in different scene
        yield return new WaitForSeconds(0.5f);
        _nightTime = FindObjectOfType<NightTime>();
        Debug.Log("nightTime");
        gameObject.SetActive(false);
    }
    public void EnableNightTime()
    {
        if (_nightTime)
        { _nightTime.Execute();
            Debug.Log("executed");
        }
    }
}
