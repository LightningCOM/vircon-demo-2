﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceHub.Conference
{
    public abstract class LiveStreamBase : MonoBehaviour
    {
         
        public UnityAction RoomJoinedCallback;

        public abstract bool IsConnected();
        public abstract bool IsInRoom();

        public abstract bool IsSpeakerPresent();
        public abstract void SendLiveStream(byte[] liveStream);
        public abstract int GetAttendeesCount();
         
    }
}

