﻿using System.Collections;
using System.Collections.Generic; 
using SpaceHub.Conference;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class LiveStreamManager : LiveStreamBase, IOnEventCallback, IMatchmakingCallbacks
{
    public int m_Streamer;
    public GameViewEncoder gameViewEncoder;
    public GameViewDecoder gameViewDecoder; 

    ConferenceConnector m_Connector
    {
        get
        {
            return PlayerLocal.Instance.Connector;
        }
    }
     
    LoadBalancingClient m_Client { get { return m_Connector.Network.Client; } }
   

    private IEnumerator Start()
    {
        while (IsConnected() == false)
        {
            yield return null;
        } 
        //m_Connector.JoinOrChangeRoom( "RealtimeStage" ); 
        //Everyone is connected to same voice room for now, changing methods soon
        while (PlayerLocal.Instance.Client.CurrentRoom == null)
        {
            yield return null;
        }

        m_Client.AddCallbackTarget(this);
        m_Connector.RoomPropertiesUpdateCallback += OnRoomPropertiesUpdated;
        m_Connector.JoinedRoomCallback += OnJoinedRoom;
        // PlayerLocal.Instance.VoiceConnection.JoinRoom( "StageRealtime" );

    }
     
    private void OnDestroy()
    {
        m_Client?.RemoveCallbackTarget(this);
        m_Connector.RoomPropertiesUpdateCallback -= OnRoomPropertiesUpdated;
        m_Connector.JoinedRoomCallback -= OnJoinedRoom;
        PlayerLocal.Instance?.VoiceConnection.LeaveRoom(); 

    }

    void OnRoomPropertiesUpdated(ExitGames.Client.Photon.Hashtable data)
    {
       // Debug.Log("OnRoomProperties Updated - Live StreamHandler");

        if (data.ContainsKey("LiveStreamKey"))
        {
             var key = (byte[])data["LiveStreamKey"];
            gameViewDecoder.Action_ProcessImageData(key);

            // PresentationCallback?.Invoke((int)data["LiveStreamKey"]);
            //  Debug.Log("LiveStreamKey" + " / " + key);
        }

    } 

    public void OnEvent(EventData photonEvent)
    {
        if (PlayerLocal.Instance == null ||
                PlayerLocal.Instance.Client == null ||
                PlayerLocal.Instance.Client.CurrentRoom == null ||
                PlayerLocal.Instance.Client.CurrentRoom.Players.ContainsKey(photonEvent.Sender) == false)
        {
            return;
        }

      //  Debug.Log($"Received code: {photonEvent.Code}"); 
    }
     

    public void OnFriendListUpdate(List<FriendInfo> friendList) {  }

    public void OnCreatedRoom() {  }

    public void OnCreateRoomFailed(short returnCode, string message) { 
    }

    public void OnJoinedRoom() { 
    }

    public void OnJoinRoomFailed(short returnCode, string message) { 
    }

    public void OnJoinRandomFailed(short returnCode, string message) { 
    }

    public void OnLeftRoom() { 
    }

    public override bool IsConnected()
    { 
        return m_Connector != null && m_Connector.Network.Client.IsConnectedAndReady;
    }

    public override bool IsInRoom()
    {
        return m_Connector != null && IsConnected() && m_Connector.Network.Client.InRoom;
    }

    public override bool IsSpeakerPresent()
    {
        return m_Client != null && m_Client.CurrentRoom != null && m_Client.CurrentRoom.GetPlayer(m_Streamer) != null;
    }
     

    public override int GetAttendeesCount()
    {
        if (m_Client == null || m_Client.IsConnectedAndReady == false || m_Client.CurrentRoom == null)
        {
            return 0;
        }

        int count = m_Client.CurrentRoom.PlayerCount;
        if (IsSpeakerPresent())
        {
            --count;
        }
        return count;
    }

    public override void SendLiveStream(byte[] liveStream)
    {
        ExitGames.Client.Photon.Hashtable table = new ExitGames.Client.Photon.Hashtable();
        table.Add("LiveStreamKey", liveStream);
        m_Client.CurrentRoom.SetCustomProperties(table);
    }

    public void SendStream(byte[] live)
    {
        if (PlayerLocal.Instance == null ||
               PlayerLocal.Instance.Client == null ||
               PlayerLocal.Instance.Client.CurrentRoom == null )
        {
            return;
        }
        //Debug.Log($"Send Live Stream: {live}");
        SendLiveStream(live);
    }
}
