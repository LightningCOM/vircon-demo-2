﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class DynamicVideoMat : MonoBehaviour
{
    VideoPlayer tvScreen;
    Shader shader;
    Material material;
    public RenderTexture renderTexture;
    new Renderer renderer;

    void Start()
    {
        tvScreen = GetComponentInChildren<VideoPlayer>();
        renderer = GetComponentInChildren<Renderer>();
        shader = Shader.Find("Universal Render Pipeline/Unlit");
        material = new Material(shader);
        renderTexture = new RenderTexture(1280, 1280, 24)
        {
            width = 1280,
            height = 1280,
            dimension = UnityEngine.Rendering.TextureDimension.Tex2D,
            graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm,
            useDynamicScale = true
        };

        bool didRenderTexCreate = renderTexture.Create();

        material.SetTexture("_BaseMap", renderTexture);
        renderer.material = material;
        tvScreen.renderMode = VideoRenderMode.RenderTexture;
        tvScreen.targetTexture = renderTexture;
    }
}