﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BallBehavior : MonoBehaviourPunCallbacks //, IPunObservable
{
    public static BallBehavior instance;
    Transform ballTransform;
    Vector3 initialPos;

    void Start()
    {
        instance = this;
        //PV = GetComponent<PhotonView>();
        ballTransform = GetComponent<Transform>();
        initialPos = ballTransform.position;
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            //BallManager.instance.KickCall(Camera.main.transform.forward);
            //GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 20);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Ball Collision: " +collision.collider.name);
        if (collision.collider.tag == "OutOfBounds")
            ReturnInitial();
        //if (collision.collider.name == "BallTriggerZone")
        //    BallManager.instance.KickCall(collision.collider.GetComponent<Transform>().forward);
    }

    public void Kick(Vector3 direction)
    {
        Debug.Log("Calling for ball kick" + direction);
        BallManager.instance.KickCall(direction);
    }
    public void ReturnInitial()
    {
        ballTransform.position = initialPos;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }
}
