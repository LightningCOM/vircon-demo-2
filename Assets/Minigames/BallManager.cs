﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    public static BallManager instance;
    private PhotonView PV;

    [SerializeField]
    private Rigidbody BallObject;
    void Start()
    {
        if(BallManager.instance == null)
        {
            instance = this;
        }
        //else
        //{
        //    Destroy(BallManager.instance.gameObject);
        //    BallManager.instance = this;
        //}
        PV = GetComponent<PhotonView>();
    }


    [PunRPC]
    void BallKick(Vector3 direction)
    {
        BallObject.AddForce(direction * 100);
    }

    public void KickCall(Vector3 direction)
    {
        //if (PV.IsMine)
        //{
        PV.RPC("BallKick", RpcTarget.AllBuffered, direction);
        //}
    }
}
