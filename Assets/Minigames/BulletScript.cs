﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class BulletScript : MonoBehaviourPun
{
    public static BulletScript instance;
    public Transform SpawnPoint;

    private void Start()
    {
        if (BulletScript.instance == null)
        {
            BulletScript.instance = this;
        }

        else if (BulletScript.instance != null)
        {
            Destroy(this.gameObject.GetComponent<BulletScript>());
            //Destroy(this.gameObject);
        }
        //PV = GetComponent<PhotonView>();
        //PhotonNetwork.AllocateViewID(PV);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Shooting Script: " + transform.parent.forward);
            ShootingManager.instance.ShootCall(SpawnPoint.transform.position, -transform.TransformDirection(transform.forward));
        }
    }
}
