﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour
{
    [SerializeField]
    private TextMesh scoreboard;

    int score;
    void Start()
    {
        score = 0;
        scoreboard.text = score.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Sphere")
        {
            other.GetComponent<BallBehavior>().ReturnInitial();
            score++;
            scoreboard.text = score.ToString();
        }
    }
}
