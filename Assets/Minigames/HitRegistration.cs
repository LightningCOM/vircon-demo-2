﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitRegistration : MonoBehaviour
{
    [SerializeField]
    private GameObject HitText;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Bullet")
        {
            StopAllCoroutines();
            Destroy(collision.collider.gameObject);
            HitText.SetActive(true);
            StartCoroutine(TextDuration());
        }   
    }

    IEnumerator TextDuration()
    {
        yield return new WaitForSeconds(2);
        HitText.SetActive(false);
        yield return null;
    }
}
