﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBallAction : MonoBehaviour
{

    bool kicked;
    private void Start()
    {
        kicked = false;
    }
    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("PlayerAction:" + collision.collider.name);
        if (collision.collider.name == "Sphere" && kicked == false)
        {
            BallBehavior.instance.Kick(-transform.TransformDirection(transform.forward));
            kicked = true;
        }       
    }

    public void OnCollisionExit(Collision collision)
    {
        if(collision.collider.name == "Sphere")
        {
            kicked = false;
        }        
    }
    //public void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("PlayerTriggerAction: " + other.name);
    //}
}
