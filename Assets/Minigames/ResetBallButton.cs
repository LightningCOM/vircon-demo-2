﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBallButton : MonoBehaviour
{
    [SerializeField]
    private Material mat;

    private void OnMouseOver()
    {
        mat.color = Color.yellow;
        if (Input.GetMouseButton(0))
        {
            BallBehavior.instance.ReturnInitial();
        }
    }
    private void OnMouseExit()
    {
        mat.color = Color.red;
    }
}
