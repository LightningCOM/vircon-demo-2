﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ShootingManager : MonoBehaviour
{
    public static ShootingManager instance;
    [SerializeField]
    private GameObject Bullet;

    PhotonView PV;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        PV = GetComponent<PhotonView>();
    }

    [PunRPC]
    void Shoot(Vector3 Position, Vector3 Direction)
    {
        Debug.Log("Shooting... towards: " + Direction);
        GameObject BulletShot = Instantiate(Bullet);
        BulletShot.transform.position = Position;
        BulletShot.GetComponent<Rigidbody>().AddForce(Direction * 100);
    }

    public void ShootCall(Vector3 position, Vector3 Direction)
    {
        Debug.Log("Shooting Call: " + Direction);
        PV.RPC(nameof(Shoot), RpcTarget.All, position, Direction);      
    }
}
