﻿
using UnityEngine;

public class OpenChest : MonoBehaviour
{
    [SerializeField] private GameObject Popup;
    public void Execute()
    {
        transform.rotation = Quaternion.Euler(new Vector3(-40, 90, 0));
        Popup.SetActive(true);
    }
}
