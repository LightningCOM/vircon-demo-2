﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceHub.Playfab;

public class PhoneScreen : MonoBehaviour
{
    [SerializeField] PlayerEventSender _eventSender;
    // Start is called before the first frame update
    public Collider coll;
    private MeshRenderer renderer;
    public MeshRenderer renderer2;
    public GameObject tutorialText;


    public List<Color> colorList;
    public List<Texture> textureList;
 
    bool isRaffleStarted = false;

    void Start()
    {
       //  coll = GetComponent<Collider>();
         renderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isRaffleStarted == false)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (coll.Raycast(ray, out hit, 100))
            {
                Debug.Log("Phone Screen Clicked!");
                SendPlayfabEvent();
                isRaffleStarted = true;
                tutorialText.SetActive(false);
                StartCoroutine(RaffleSystemCountDown(3, 0.05f)); // Start Raffle System CountDown

            }
        }
    }

    IEnumerator RaffleSystemCountDown(float seconds, float decrement) {
        float counter = seconds;
        while (counter > 0) {
                GenerateRandomColors();
            yield return new WaitForSeconds (decrement);
            counter -= decrement;
        } 
        GenerateRandomTexture();
    }
      
    // Generate random colors
    // Then generate random texture
    void GenerateRandomColors(){
        renderer.material.mainTexture = null; // Remove texture and generate a new color
        renderer2.material.mainTexture = null;
        var randomIndex = Random.RandomRange(0, colorList.Count);
        renderer.material.color = colorList[randomIndex];
        renderer2.material.color = colorList[randomIndex];
    }

    void GenerateRandomTexture(){
        var randomIndex = Random.RandomRange(0, textureList.Count);
        renderer.material.color = Color.white;
        renderer.material.mainTexture = textureList[randomIndex]; 
        renderer2.material.color = Color.white;
        //renderer2.material.mainTexture = textureList[randomIndex]; 
        isRaffleStarted = false;
    }

    void SendPlayfabEvent()
    {
        if (_eventSender)
        {
           // _eventSender.SendEvent();
        }
    }
}
