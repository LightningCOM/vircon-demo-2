mergeInto(LibraryManager.library, {

   
  OpenModal: function (str) {
    window.alert(Pointer_stringify(str));
  },

   toggleLive: function (url) {
    // get the clock
    var myClock = document.getElementById('clock');

    // get the current value of the clock's display property
    var displaySetting = myClock.style.display;
     

    // now toggle the clock and the button text, depending on current state
    if (displaySetting == 'block') {
      // clock is visible. hide it
      myClock.style.display = 'none';
      // change button text 
      document.getElementById("livestream").src = "";
    }
    else {
      // clock is hidden. show it
      myClock.style.display = 'block';
      document.getElementById("livestream").src = Pointer_stringify(url);
      // change button text 
    }
  }



});