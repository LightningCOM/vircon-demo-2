﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using System.IO;
public class PunManager : MonoBehaviourPunCallbacks, IMatchmakingCallbacks
{
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
        
    }

    public void OnDestroy()
    {
        PhotonNetwork.Disconnect();
    }


    public override void OnConnectedToMaster()
    {
        Debug.Log("On Connected to Master Server! Pun Manager");
        // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = false;
        roomOptions.MaxPlayers = 200;
        PhotonNetwork.JoinOrCreateRoom(SceneManagerHelper.ActiveSceneName, roomOptions, TypedLobby.Default);
    }
}
