﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using System;

namespace SpaceHub.Conference
{


    public class ConferenceRoomManager : MonoBehaviour
    {
        public static ConferenceRoomManager Instance;

        static string m_RoomToLoad;
        static string m_PreviousRoom;
        static string m_SpawnPointId;

        const string ExpoManagerScene = "Expo";
        
        public string DefaultExpoRoom = "ExpoRoomMain";
        public bool isDefaultRoomAddressable = false;

        public UnityAction<string, string> SceneChangedCallback;

        Scene m_CurrentRoomScene;
        AsyncOperationHandle<SceneInstance> m_CurrentRoomSceneInstance;
        static bool isCurrentSceneAddressable = false;
        static bool isPreviousSceneAddressable = false;

        public string CurrentRoomName { get { return m_RoomToLoad; } }

        private void Awake()
        {
            Instance = this;

            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;

            if ( string.IsNullOrEmpty( m_RoomToLoad ) )
            {
                m_RoomToLoad = DefaultExpoRoom;
            }
            if( string.IsNullOrEmpty( m_SpawnPointId ) )
            {
                m_SpawnPointId = SpawnPoint.Default;
            }
        }
        private void Start()
        {
            LoadRoom( m_RoomToLoad, m_SpawnPointId, null, isDefaultRoomAddressable );
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        public static void LoadRoom( string sceneName, string spawnPoint, string returnSpawnPoint, bool isAddressable = false, UnityAction callback = null )
        {
            if (isAddressable)
            {
                Instance.StartCoroutine(LoadAddressableRoom(sceneName, spawnPoint, returnSpawnPoint, isAddressable, callback));
                return;
            }
            Debug.Log( "Load room: " + sceneName + " - At spawnpoint: " + spawnPoint );
            isPreviousSceneAddressable = isCurrentSceneAddressable;
            isCurrentSceneAddressable = isAddressable;
            ConnectingOverlay.Show();

            m_RoomToLoad = sceneName;
            m_SpawnPointId = spawnPoint;


            if( Instance == null )
            {
                print("No Expo Present");
                SceneManager.LoadScene( ExpoManagerScene, LoadSceneMode.Single );
                return;
            }

            if ( callback != null )
            {
                Instance.StartCoroutine( Instance.WaitForLoadAndConnect( callback ) );
            }

            if (isPreviousSceneAddressable)
            {
                print($"1 - IsAddressable");
                PlayerLocal.Instance.Connector.IsMessageQueueRunning = false;
                Addressables.UnloadSceneAsync(Instance.m_CurrentRoomSceneInstance);
            }

            else if( Instance.m_CurrentRoomScene.IsValid() && Instance.m_CurrentRoomScene.isLoaded )
            {
                print($"2 - IsNotAddressable, with valid");
                PlayerLocal.Instance.Connector.IsMessageQueueRunning = false;

                Debug.Log( "Unloading Scene: " + Instance.m_CurrentRoomScene.name );
                m_PreviousRoom = Instance.m_CurrentRoomScene.name;

                SceneManager.UnloadSceneAsync( Instance.m_CurrentRoomScene );
            }
            else
            {
                if (isAddressable)
                {
                    print($"3 - IsAddressable");
                    Instance.m_CurrentRoomSceneInstance = Addressables.LoadSceneAsync(m_RoomToLoad, LoadSceneMode.Additive);
                    Instance.m_CurrentRoomSceneInstance.Completed -= OnAddressableLoadComplete;
                    Instance.m_CurrentRoomSceneInstance.Completed += OnAddressableLoadComplete;
                }
                else
                {
                    print($"3 - IsNotAddressable");
                    SceneManager.LoadSceneAsync( m_RoomToLoad, LoadSceneMode.Additive );
                }
            }

            BackUI.Instance?.RemoveLast();
            if( string.IsNullOrEmpty( returnSpawnPoint ) == false )
            {
                BackUI.Instance?.AddBackData( () => { LoadRoom( m_PreviousRoom, returnSpawnPoint, null ); }, "Back to Lobby", m_PreviousRoom );
            }
        }

        static IEnumerator LoadAddressableRoom(string sceneName, string spawnPoint, string returnSpawnPoint, bool isAddressable = false, UnityAction callback = null)
        {
            Debug.Log("Load room: " + sceneName + " - At spawnpoint: " + spawnPoint);
            isPreviousSceneAddressable = isCurrentSceneAddressable;
            isCurrentSceneAddressable = isAddressable;

            m_RoomToLoad = sceneName;
            m_SpawnPointId = spawnPoint;

            if (Instance == null)
            {
                print("No Expo Present");
                SceneManager.LoadScene(ExpoManagerScene, LoadSceneMode.Single);
                yield break;
            }

            if (callback != null)
            {
                Instance.StartCoroutine(Instance.WaitForLoadAndConnect(callback));
            }

            if (isPreviousSceneAddressable)
            {
                print($"1 - IsAddressable");
                PlayerLocal.Instance.Connector.IsMessageQueueRunning = false;
                Addressables.UnloadSceneAsync(Instance.m_CurrentRoomSceneInstance);
            }

            else if (Instance.m_CurrentRoomScene.IsValid() && Instance.m_CurrentRoomScene.isLoaded)
            {
                print($"2 - IsNotAddressable, with valid");
                PlayerLocal.Instance.Connector.IsMessageQueueRunning = false;

                Debug.Log("Unloading Scene: " + Instance.m_CurrentRoomScene.name);
                m_PreviousRoom = Instance.m_CurrentRoomScene.name;

                SceneManager.UnloadSceneAsync(Instance.m_CurrentRoomScene);
            }
            else
            {
                if (isAddressable)
                {
                    print($"3 - IsAddressable");
                    AddressableStateOverlay.Show();

                    AddressableStateOverlay.Instance.StateText.text = "Checking for updates...";
                    var checkUpdatesOperation = Addressables.CheckForCatalogUpdates();
                    while (!checkUpdatesOperation.IsDone)
                    {
                        yield return new WaitForSeconds(0.1f);
                    }

                    AddressableStateOverlay.Instance.StateText.text = "Analyzing cache...";
                    var checkCacheOperation = Addressables.GetDownloadSizeAsync(m_RoomToLoad);
                    while (!checkCacheOperation.IsDone)
                    {
                        yield return new WaitForSeconds(0.1f);
                    }

                    if (checkCacheOperation.Result > 0)
                    {
                        AddressableStateOverlay.Instance.StateText.text = "Downloading level...";
                        var downloadOperation = Addressables.DownloadDependenciesAsync(m_RoomToLoad);
                        while (!downloadOperation.IsDone)
                        {
                            yield return new WaitForSeconds(0.1f);
                            AddressableStateOverlay.Instance.StateText.text = $"Downloading level... ({Math.Round(downloadOperation.GetDownloadStatus().DownloadedBytes * 0.000001, 2)}MB/{Math.Round(downloadOperation.GetDownloadStatus().TotalBytes * 0.000001, 2)}MB)";
                        }
                    }

                    AddressableStateOverlay.Instance.StateText.text = "Loading level...";
                    Instance.m_CurrentRoomSceneInstance = Addressables.LoadSceneAsync(m_RoomToLoad, LoadSceneMode.Additive);
                    Instance.m_CurrentRoomSceneInstance.Completed -= OnAddressableLoadComplete;
                    Instance.m_CurrentRoomSceneInstance.Completed += OnAddressableLoadComplete;
                }
                else
                {
                    print($"3 - IsNotAddressable");
                    SceneManager.LoadSceneAsync(m_RoomToLoad, LoadSceneMode.Additive);
                }
            }

            BackUI.Instance?.RemoveLast();
            if (string.IsNullOrEmpty(returnSpawnPoint) == false)
            {
                BackUI.Instance?.AddBackData(() => { LoadRoom(m_PreviousRoom, returnSpawnPoint, null); }, "Back to Lobby", m_PreviousRoom);
            }
        }

        IEnumerator LoadAddressableLevel()
        {
            print($"3 - IsAddressable");
            AddressableStateOverlay.Show();

            AddressableStateOverlay.Instance.StateText.text = "Checking for updates...";
            var checkUpdatesOperation = Addressables.CheckForCatalogUpdates();
            while (!checkUpdatesOperation.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            AddressableStateOverlay.Instance.StateText.text = "Analyzing cache...";
            var checkCacheOperation = Addressables.GetDownloadSizeAsync(m_RoomToLoad);
            while (!checkCacheOperation.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            if (checkCacheOperation.Result > 0)
            {
                AddressableStateOverlay.Instance.StateText.text = "Downloading level...";
                var downloadOperation = Addressables.DownloadDependenciesAsync(m_RoomToLoad);
                while (!downloadOperation.IsDone)
                {
                    yield return new WaitForSeconds(0.1f);
                    AddressableStateOverlay.Instance.StateText.text = $"Downloading level... ({Math.Round(downloadOperation.GetDownloadStatus().DownloadedBytes * 0.000001, 2)}MB/{Math.Round(downloadOperation.GetDownloadStatus().TotalBytes * 0.000001, 2)}MB)";
                }
            }

            AddressableStateOverlay.Instance.StateText.text = "Loading level...";
            Instance.m_CurrentRoomSceneInstance = Addressables.LoadSceneAsync(m_RoomToLoad, LoadSceneMode.Additive);
            Instance.m_CurrentRoomSceneInstance.Completed -= OnAddressableLoadComplete;
            Instance.m_CurrentRoomSceneInstance.Completed += OnAddressableLoadComplete;
        }

        private static void OnAddressableLoadComplete(AsyncOperationHandle<SceneInstance> obj)
        {
            SceneManager.SetActiveScene(obj.Result.Scene);
            PlayerLocal.Instance.Connector.IsMessageQueueRunning = true;
            AddressableStateOverlay.Hide();
            print($"{obj.Result.Scene.name} - Active");
        }

        IEnumerator WaitForLoadAndConnect( UnityAction callback )
        {
            while( m_RoomToLoad != m_CurrentRoomScene.name )
            {
                yield return null;
            }

            yield return null;

            while( PlayerLocal.Instance.Client.CurrentRoom == null || PlayerLocal.Instance.Client.CurrentRoom.Name != m_RoomToLoad || PlayerLocal.Instance.Client.IsConnectedAndReady == false )
            {
                yield return null;
            }

            callback?.Invoke();
        }

        public string GetCurrentRoomName()
        {
            return m_RoomToLoad;
        }

        void OnSceneUnloaded( Scene scene )
        {
            if( string.IsNullOrEmpty( m_RoomToLoad ) == false )
            {
                if (isCurrentSceneAddressable)
                {
                    StartCoroutine(LoadAddressableLevel());
                }
                else SceneManager.LoadSceneAsync(m_RoomToLoad, LoadSceneMode.Additive);
            }
        }

        void OnSceneLoaded( Scene scene, LoadSceneMode mode )
        {
            Debug.Log( "Loaded Scene " + scene.name + ". Mode: " + mode.ToString() );
            if( mode != LoadSceneMode.Additive)
            {
                return;
            }

            if (isCurrentSceneAddressable)
            {
                print("DEBUG Addressable" + m_CurrentRoomScene + " " + m_CurrentRoomSceneInstance);
            }
            else
            {
                m_CurrentRoomScene = scene;
                SceneManager.SetActiveScene( m_CurrentRoomScene );
            }
            LightProbes.Tetrahedralize();

            var spawnPoint = SpawnPoint.GetSpawnPositionById( m_SpawnPointId );

            PlayerLocal.Instance.GoToAndLook( spawnPoint.transform, true, true );

            Instance.SceneChangedCallback?.Invoke( m_PreviousRoom, m_RoomToLoad );

            StartCoroutine( JoinRoomRoutine( m_RoomToLoad ) );

            PlayerLocal.Instance.Connector.IsMessageQueueRunning = true;
        }

        IEnumerator JoinRoomRoutine( string roomName )
        {
            Debug.Log( "Join Room Routine. Is Connected and ready: " + PlayerLocal.Instance.Client.IsConnectedAndReady );

            while( PlayerLocal.Instance.Client.IsConnectedAndReady == false )
            {
                yield return null;
            }

            Debug.Log( "Join room " +  roomName);

            PlayerLocal.Instance.Connector.JoinOrChangeRoom( roomName );
        }

    }
}
