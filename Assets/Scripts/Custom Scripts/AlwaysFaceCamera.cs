﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysFaceCamera : MonoBehaviour
{
    Camera mainCam;

    public void Start()
    {
        mainCam = Camera.main;
    }

    public void Update()
    {
        this.transform.rotation = mainCam.transform.rotation;
    }
}
