﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class AssetInitialization : MonoBehaviour
{
    public static AssetInitialization Instance;
    public string sceneCatalogUrl = "https://vircon-demo-assets.s3.ap-southeast-1.amazonaws.com/WebGL/catalog_VirconDemoScenes.json";

    IEnumerator Start()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
        }
        Instance = this;

        Addressables.ClearResourceLocators();
        var loadCatalogOp = Addressables.LoadContentCatalogAsync(sceneCatalogUrl);
        while (!loadCatalogOp.IsDone)
        {
            yield return new WaitForSeconds(0.2f);
        }
    }
}
