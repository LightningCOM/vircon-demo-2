﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SpaceHub.Conference
{
    public class GameConsole : MonoBehaviour
    {
        [Header("Object Components")]
        StageHandlerBase m_connection;
        [SerializeField] TextMeshProUGUI _passwordText;
        [SerializeField] string[] _passwords;
        public bool ConsoleInUse = false;
        [Header("Events")]
        [SerializeField] VoidEvent _consoleOpened;
        [SerializeField] VoidEvent _consoleUIOpened;
        [SerializeField] VoidEvent _consoleClosed;
        [SerializeField] VoidEvent _passwordCorrect;
        [SerializeField] VoidEvent _passwordIncorrect;
        public GameObject Door;
        public GameObject CorrectPanel;
        public GameObject PlayerBubble;
        public GameObject InputField;

        private bool _activity1Done;
        private void Awake()
        {
            m_connection = FindObjectOfType<StageHandlerBase>();
            m_connection.ConsoleTextCallback += OnGetConsolePasswordTextUpdate;
            m_connection.ConsolePasswordCheckedCallback += OnGetConsolePasswordChecked;
            m_connection.ConsoleIsActiveCallback += OnConsoleIsActiveUpdate;
        }

        public void UseConsole()
        {
            if (ConsoleInUse)
            {
                return;
            }

            _consoleUIOpened.Raise();
            m_connection.UpdateConsoleIsActive(true);
        }

        public void OnConsolePasswordChanged(string p_password)
        {
            SendPasswordTextUpdate(p_password);
        }

        public void OnPasswordSubmitted(string p_password)
        {
            bool passwordIsCorrect = CheckPassword(p_password);
            m_connection.SendConsolePasswordResult(passwordIsCorrect);
        }

        private bool CheckPassword(string p_password)
        {
            p_password = p_password.ToLower();
            if (_activity1Done)
            {
                if (p_password == _passwords[1])
                {
                    return true;
                }
                return false;
            } else
            {
                if (p_password == _passwords[0])
                {
                    _activity1Done = true;
                    return true;
                }
                return false;
            }
            
        }

        private void SendPasswordTextUpdate(string p_password)
        {
            m_connection.UpdateConsolePassword(p_password);
        }

        private void ActivateConsole()
        {
            ConsoleInUse = true;
            _consoleOpened.Raise();
        }

        private void DeactivateConsole()
        {
            ConsoleInUse = false;
            _consoleClosed.Raise();
            PlayerBubble.SetActive(false);
            InputField.SetActive(false);
        }

        public void OnConsoleUIClosed()
        {
            m_connection.UpdateConsoleIsActive(false);
        }

        public void OnConsoleIsActiveUpdate(bool p_consoleIsActive)
        {
            if (p_consoleIsActive)
            {
                ActivateConsole();
            } else
            {
                DeactivateConsole();
            }
        }

        private void OnGetConsolePasswordTextUpdate(string p_password)
        {
            _passwordText.text = p_password;
        }

        private void OnGetConsolePasswordChecked(bool p_passwordIsCorrect)
        {
            if (p_passwordIsCorrect)
            {
                _passwordCorrect.Raise();
            }
            else
            {
                _passwordIncorrect.Raise();
            }
        }
    }
}
