﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameConsoleUI : MonoBehaviour
{
    [Header("Object Components")]
    [SerializeField] TMP_InputField _passwordInputField;
    [Header("Events")]
    [SerializeField] StringEvent _passwordChanged;
    [SerializeField] StringEvent _passwordSubmitted;

    public void OnPasswordChanged()
    {
        _passwordChanged.Raise(_passwordInputField.text);
    }

    public void OnConsolePasswordSubmitted()
    {
        _passwordSubmitted.Raise(_passwordInputField.text);
    }

    public void EnableInputField(bool enable)
    {
        _passwordInputField.interactable = enable;
    }
}
