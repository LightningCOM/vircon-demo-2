﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using UnityEngine.Networking;

public class ObjectFaceCamera : MonoBehaviour
{
    private Transform _cameraTransform;
    private Transform _objectTransform;

    private void Start()
    {
        if (Camera.main != null) _cameraTransform = Camera.main.transform;
        _objectTransform = transform;
    }

    private void FixedUpdate()
    {
        if(_cameraTransform != null)
            _objectTransform.rotation = _cameraTransform.rotation;
    }
}
