﻿using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine;

public class iFrameHandler : MonoBehaviour
{
#if UNITY_WEBGL && !UNITY_EDITOR

    [DllImport("__Internal")]
    private static extern void OpenModal(string str);

    [DllImport("__Internal")]
    private static extern void toggleLive(string url);

#endif

    public string url = "";

    public void SendToJS()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        toggleLive(url);
#endif
    }


    private void OnMouseDown()
    {
        OpenIFrame();
    }
    public void OpenIFrame()
    {
        if (string.IsNullOrEmpty(url)) return;
        print($"Loading {url}");
        SendToJS();
    }
}
