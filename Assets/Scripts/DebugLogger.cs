﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLogger : MonoBehaviour
{
    public static DebugLogger instance;
    [SerializeField]
    private Text DebugText;
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DebugText.text = "Debug Logger started";
    }

    public void LogDebug(string log)
    {
        DebugText.text = log;
    }
}
