﻿using UnityEngine;

public class HallData : MonoBehaviour
{
    public static HallData Instance;
    public Sprite[] Booth1Banners;
    public Sprite[] Booth2Banners;
    public Sprite[] Booth3Banners;
    public Sprite[] Booth4Banners;
    public Sprite[] Booth5Banners;
    public Sprite[] Booth6Banners;
    public Sprite[] Booth7Banners;
    public Sprite[] Booth8Banners;
    public Sprite[] Booth9Banners;
    public Sprite[] Booth10Banners;
    public Sprite[] Booth11Banners;
    public Sprite[] Booth12Banners;
    public Sprite[] Booth13Banners;
    public Sprite[] Booth14Banners;
    public Sprite[] Booth15Banners;
    public Sprite[] Booth16Banners;
    public Sprite[] Booth17Banners;
    public Sprite[] Booth18Banners;
    public Sprite[] Booth19Banners;
    public Sprite[] Booth20Banners;
    public Sprite[][] BoothList;
    public string[] BoothNames;
    public string[] BoothLinks;
    private void Awake()
    {
        Instance = this;
        BoothList = new Sprite[][] { Booth1Banners, Booth2Banners, Booth3Banners , Booth4Banners , Booth5Banners , Booth6Banners , Booth7Banners , Booth8Banners , Booth9Banners , Booth10Banners , Booth11Banners , Booth12Banners , Booth13Banners , Booth14Banners , Booth15Banners , Booth16Banners , Booth17Banners , Booth18Banners , Booth19Banners , Booth20Banners };
    }
}
