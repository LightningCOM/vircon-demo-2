﻿using System;
using UnityEngine;

namespace SpaceHub.Conference
{
    public class LoadSceneInteractable : MonoBehaviour
    {
        public string SceneName;
        public string SpawnPointId;
        public string ReturnSpawnPointId;
        public bool isVideoReady;
        public bool isBooth;
        public bool isAddressableScene;

        private void Awake()
        {
            GetComponentInChildren<ConferenceInteractable>().SelectExitCallback?.AddListener(LoadScene);
        }

        Vector3 GetMovementPosition()
        {
            return transform.position + transform.forward;
        }

        void LoadScene()
        {
            ConnectingOverlay.Instance.ToggleOverlay(true);
            PlayerLocal.Instance.SendEnterPortal(GetMovementPosition());
            if (isBooth)
            {
                var currentBoothData = CurrentBoothData.Instance;
                currentBoothData.BoothId = ReturnSpawnPointId;
                var boothId = Convert.ToInt32(ReturnSpawnPointId.Replace("booth ", ""));
                var hallDataIndex = boothId > 20 ? boothId - 21 : boothId - 1;
                var hallData = HallData.Instance;
                //currentBoothData.BoothImages = hallData.BoothList[hallDataIndex];
                //currentBoothData.IsVideoReady = isVideoReady;
                //currentBoothData.BoothLink = hallData.BoothLinks[hallDataIndex];
                currentBoothData.CompanyName = hallData.BoothNames[hallDataIndex];
                ReturnSpawnPointId = null;
                SceneName += " " + boothId;
            }
            ConferenceRoomManager.LoadRoom(SceneName, SpawnPointId, ReturnSpawnPointId, isAddressableScene);
        }
    }
}
