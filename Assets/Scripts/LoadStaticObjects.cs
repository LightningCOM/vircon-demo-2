﻿using SpaceHub.Conference;
using System.Collections;
using UnityEngine;

public class LoadStaticObjects : MonoBehaviour
{
    [SerializeField] GameObject Island1;
    private GameObject _island1;
    [SerializeField] GameObject Island2;
    private GameObject _island2;
    private void Awake()
    {
        _island1 = Instantiate(Island1);
        _island2 = Instantiate(Island2);
        StartCoroutine(EnableIsland2(false));
        StartCoroutine(UpdateLocation());
    }

    private IEnumerator UpdateLocation()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            PlayerLocal.Instance.ForceSendCurrentPositionAndRotationWithHighAccuracy(0);
        }
    }

    private IEnumerator EnableIsland2(bool enable)
    {
        yield return new WaitForSeconds(1.2f);
        _island2.SetActive(enable);
    }

    private void OnDestroy()
    {
        if (_island1)
            Destroy(_island1);
        if (_island2)
            Destroy(_island2);
    }

    public void EnableIsland2()
    {
        StartCoroutine(EnableIsland2(true));
    }

}
