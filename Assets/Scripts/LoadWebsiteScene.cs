﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using MarksAssets.LaunchURLWebGL;

public class LoadWebsiteScene : MonoBehaviour
{
    [SerializeField]
    private string url;

    [SerializeField]
    private bool DelayLoad;

    public void OpenUrl()
    {
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (!DelayLoad)
            {
                LaunchURLWebGL.instance.launchURLSelf(url);
            }
            else
            {
                StartCoroutine(Delay());
            }
            
        }
        else
        {
            Application.OpenURL(url);
            Application.Quit();
        }
        
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(3);
        LaunchURLWebGL.instance.launchURLSelf(url);
    }
}
