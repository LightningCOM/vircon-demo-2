﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenGaphicsSettings : MonoBehaviour
{
    [SerializeField]
    private Button[] qualityOptions;
    public void SetQualityLevel(int level)
    {
        QualitySettings.SetQualityLevel(level, true);
        Application.targetFrameRate = 60;
        Debug.Log("Quality Level: " + QualitySettings.GetQualityLevel() + QualitySettings.GetRenderPipelineAssetAt(level));
    }

    public void QualityHighlight()
    {
        foreach(Button btn in qualityOptions)
        {
            btn.image.color = btn.colors.normalColor;
        }

        qualityOptions[QualitySettings.GetQualityLevel()].image.color = Color.green;
    }
}
