﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

namespace SpaceHub.Playfab
{
    public class PlayerEventSender : PlayfabEventSender
    {
        public override void SendEvent()
        {
			PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest
			{
				EventName = _playfabEvent.eventName,
				Body = new Dictionary<string, object>()
			{
				{ "event_name", _playfabEvent.eventName }
			}
			}, null, null);
			Debug.Log(string.Format("{0} {1}", "Sent player event ", _playfabEvent.eventName));
		}
    }
}
