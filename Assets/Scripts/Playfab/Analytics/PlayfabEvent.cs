﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceHub.Playfab
{
    [CreateAssetMenu(fileName = "Playfab Event", menuName = "Playfab/Analytics/Playfab Event")]
    public class PlayfabEvent : ScriptableObject
    {
        [SerializeField] string _eventName;

        public string eventName { get { return _eventName; } }
    }
}
