﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SpaceHub.Playfab
{
    public abstract class PlayfabEventSender : MonoBehaviour
    {
        [SerializeField] protected bool _sendOnStart;
        [SerializeField] protected PlayfabEvent _playfabEvent;

        private void Start()
        {
            if (_sendOnStart)
            {
                SendEvent();
            }
        }

        public abstract void SendEvent();
    }
}
