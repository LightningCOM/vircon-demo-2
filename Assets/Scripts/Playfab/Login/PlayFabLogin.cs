﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using SpaceHub.Conference;

namespace SpaceHub.Playfab
{
    public class PlayFabLogin : MonoBehaviour
    {
        [SerializeField] PlayerEventSender _playerEventSender;
        [SerializeField] UiFunnelManager _uiFunnelManager;

        public void LoginWithPlayfab()
        {
            if (PlayFabManager.IsLoggedIn == false)
            {
                PlayFabManager.Login(OnLoginSuccess, OnLoginFailed);
            }
        }

		private void OnLoginSuccess(LoginResult result)
		{
            _playerEventSender.SendEvent();
            _uiFunnelManager.NextScene();
		}

		private void OnLoginFailed(PlayFabError error)
		{
			Debug.Log("ERROR: Login Failed. Verify Title ID is set correctly.");
            Debug.Log(error.GenerateErrorReport());
		}
	}
}