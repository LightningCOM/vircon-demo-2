﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26.Common;
using Proyecto26;
using Newtonsoft.Json;
using System;

[Serializable]
class ServerItem
{
    public string name;
    public string type;
    public VisualData data;
}

public class SaveVisualData : MonoBehaviour
{
    [SerializeField]
    private string GetUrl, PostUrl;
    public static SaveVisualData instance;
    /*private string url = "http://localhost:1337/api/remote-config/getAllData";*/
    
    private ServerItem SI;
    private Dictionary<string, string> visualData;
    private Dictionary<string, Color32> colorData;

    void Start()
    {
        visualData = new Dictionary<string, string>();
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        getAllData();
    }

    public void getAllData()
    {
        Debug.Log("Gathering Data");
        visualData = new Dictionary<string, string>();
        colorData = new Dictionary<string, Color32>();
        Debug.Log("Is Visual Data null: " + (visualData == null));
        RestClient.Get(GetUrl).Then(
            response =>
            {
                Debug.Log("RESPONSE GOTTEN! Processing Data\nIs Visual Data Null: " + (visualData == null));
                var res = JsonConvert.DeserializeObject<List<ServerItem>>(response.Text);
                foreach (var test in res)
                {
                    if (test.name == "visualData")
                    {
                        string acquiredData = "AcquriedData:";
                        foreach (KeyValuePair<string, string> vd in test.data.visuals)
                        {
                            visualData[vd.Key] = vd.Value;
                            acquiredData += "\nKey: " + vd.Key + " || value: " + vd.Value;
                        }
                        foreach (KeyValuePair<string, Color32> color in test.data.colors)
                        {
                            colorData[color.Key] = color.Value;
                        }
                        Debug.Log(acquiredData);
                    }
                }
            }, error => {
                Debug.Log("Get Data Error. " +  error);
            });
    }
    VisualData servData;
    public VisualData ServerData()
    {
        if(servData == null)
        {
            servData = new VisualData();
        }
        //VisualData servData = new VisualData();
        servData.name = "DataFromServer";
        servData.companyName = "CompanyServer";
        servData.visuals = new Dictionary<string, string>();
        servData.colors = new Dictionary<string, Color32>();
        foreach (KeyValuePair<string, string> vd in visualData)
        {
            servData.visuals[vd.Key] = vd.Value;
            Debug.Log("Key: " + vd.Key + "\nvalue: " + vd.Value);
        }
        foreach (KeyValuePair<string, Color32> color in colorData)
        {
            servData.colors[color.Key] = color.Value;
        }
        return servData;
    }
    private void postData()
    {
        Debug.Log("POSTING");
        Debug.Log("SEREIALIZED OBJECT:\n" +JsonConvert.SerializeObject(SI));
        RestClient.Request(new RequestHelper
        {
            Uri = PostUrl,
            Method = "POST",
            Timeout = 10,
            BodyString = "{ \"data\":" +JsonConvert.SerializeObject(SI) + "}",
            Retries = 5
        }).Then(response =>
        {
            Debug.Log("POST SUCCESS");
            getAllData();
        }).Catch(error =>
        {
            //var error = err as RequestException;
            Debug.Log(error + "post failed... attempting put");
            putData("{ \"data\":" + JsonConvert.SerializeObject(SI) + "}");
        });
    }

    private void putData(string data)
    {
        Debug.Log("PUTTING");
        RestClient.Request(new RequestHelper
        {
            Uri = PostUrl + "/8",
            Method = "PUT",
            Timeout = 10,
            BodyString = data,
            Retries = 5
        }).Then(response =>
        {
            Debug.Log("PUT SUCCESS");
            getAllData();
        }).Catch(error =>
        {
            Debug.Log(error + " post and put failed.");
        });
    }

    public void ApplyDataToSend(VisualData vData)
    {
        SI = new ServerItem();
        SI.name = "visualData";
        SI.type = "vData";
        SI.data = new VisualData();

        SI.data.name = "GuestName";
        SI.data.companyName = "Taktyl";
        SI.data.visuals = new Dictionary<string, string>();
        SI.data.colors = new Dictionary<string, Color32>();
        foreach(KeyValuePair<string, string> vd in vData.visuals)
        {
            SI.data.visuals[vd.Key] = vd.Value;
        }

        foreach(KeyValuePair<string, Color32> vd in vData.colors)
        {
            SI.data.colors[vd.Key] = vd.Value;
        }

        postData();
    }
}
