﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SpaceHub.Playfab;

namespace SpaceHub.Conference
{
    public class SpacehubGuestLogin : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] VirconSettings _virconSettings;
        [SerializeField] TMP_InputField _requirePasswordField;
        [SerializeField] PlayFabLogin _playfabLogin;
        bool _requiresPassword = false;
        bool _isLoggingIn = false;

        private void OnEnable()
        {
            if (_virconSettings.guestLoginPassword != "")
            {
                _requirePasswordField.gameObject.SetActive(true);
                _requiresPassword = true;
            }
        }

        public void OnLoginPressed()
        {
            if (_requiresPassword)
            {
                CheckPassword();
            } else
            {
                Login();
            }
        }

        private void CheckPassword()
        {
            if (_requirePasswordField.text == _virconSettings.guestLoginPassword)
            {
                Login();
            } else
            {
                _requirePasswordField.text = null;
                _requirePasswordField.placeholder.GetComponent<TextMeshProUGUI>().text = "Wrong Password";
            }
        }

        private void Login()
        {
            if (_isLoggingIn)
            {
                return;
            }
            _requirePasswordField.text = null;
            _requirePasswordField.placeholder.GetComponent<TextMeshProUGUI>().text = "Logging In";
            _isLoggingIn = true;
            _playfabLogin.LoginWithPlayfab();
        }
    }
}
