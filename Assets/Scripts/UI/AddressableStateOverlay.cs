﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddressableStateOverlay : MonoBehaviour
{
    public static AddressableStateOverlay Instance;

    public TextMeshProUGUI StateText;
    Canvas m_Canvas;


    public static void Show()
    {
        if (Instance != null)
        {
            Instance.StateText.text = "";
            Instance.m_Canvas.enabled = true;
        }
    }

    public static void Hide()
    {
        Instance.m_Canvas.enabled = false;
    }

    private void Awake()
    {
        Instance = this;

        m_Canvas = GetComponent<Canvas>();
    }
}
