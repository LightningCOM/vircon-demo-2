﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceHub.Conference
{
    [CreateAssetMenu(fileName ="Vircon Settings", menuName ="Vircon/Settings")]
    public class VirconSettings : ScriptableObject
    {
        [Header("Login")]
        [SerializeField] string _guestLoginPassword;

        public string guestLoginPassword { get { return _guestLoginPassword; } }
    }
}
