﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualData
{
    public string name;
    public string companyName;
    public Dictionary<string, string> visuals;
    public Dictionary<string, Color32> colors;
}
