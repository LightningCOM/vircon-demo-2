﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;
    [SerializeField]
    private InputField number1, number2, number3, number4;

    [SerializeField]
    private InputField[] key, value;

    [SerializeField]
    private ScreenLogger Log;

    private ServerEntry Data;

    //ID in the Database. Used for Putting Better to have it focused on one Entry.
    public string DataReplacementValue;
    void Start()
    {
        instance = this;  
    }

    public void MakeData()
    {
        Data = new ServerEntry();
        Data.name = "data10";
        Data.type = "test";
        Data.data = new ServerData();
        Data.data.number1 = float.Parse(number1.text);
        Data.data.number2 = float.Parse(number2.text);
        Data.data.number3 = float.Parse(number3.text);
        Data.data.number4 = float.Parse(number4.text);
        Data.data.pair = new Dictionary<string, string>();
        for(int i = 0; i < 3; i++)
        {
            Data.data.pair[key[i].text] = value[i].text;
        }
        Log.Log("Data Made!");
        DataLog();
    }

    public ServerEntry serverData { get { return Data; } }
    private void DataLog()
    {
        string dataString =
            "Name: " + Data.name +
            "\nType: " + Data.type +
            "\nNumber1: " + Data.data.number1 +
            "\nNumber2: " + Data.data.number2 +
            "\nNumber3: " + Data.data.number3 +
            "\nNumber4: " + Data.data.number4;
        foreach (KeyValuePair<string, string> kv in Data.data.pair)
        {
            dataString += "\nKey: " + kv.Key + " Value: " + kv.Value;
        }
        Log.Log(dataString);
    }
}
