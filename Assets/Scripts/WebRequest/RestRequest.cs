﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using Proyecto26.Common;
using Newtonsoft.Json;

public class RestRequest : MonoBehaviour
{
    public string GetUrl;
    public string PostUrl;

    [SerializeField]
    private ScreenLogger Log;

    List<ServerEntry> checkData;
    //So long as the data exists, then it's ok and no need to place it on a variable.
    public void GetServerData()
    {
        checkData = new List<ServerEntry>();
        RestClient.Get(GetUrl).Then(
            response =>
            {
                Log.Log("Response Recieved... Getting Data");
                Log.Log(response.Text +"\nDeserializing Data...");
                List<ServerEntry> res = JsonConvert.DeserializeObject<List<ServerEntry>>(response.Text);
                Log.Log("JSON Deserialized");
                checkData = res;
                foreach (ServerEntry test in res)
                {
                    if(test.type == "test")
                    {
                        Log.Log("Name: " + test.name + " \tNumber1: " + test.data.number1 + " \tNumber2: " + test.data.number2);
                    }
                }
                Log.Log("Foreach Passed");
            }, error => {
                Log.Log("Get Data Error. " + error);
            });
    }

    public void DataCheck()
    {
        if(checkData == null)
        {
            return;
        }
        foreach(ServerEntry data in checkData)
        {
            Log.Log("Name: " + data.name + "\tNumber1: " + data.data.number1);
        }
        Log.Log("Data checked. Clearing checkData. Get Data again if needed.");
        checkData.Clear();
    }
    //Be sure Data is made first before Posting;
    public void PostData()
    {
        if(DataManager.instance.serverData == null)
        {
            return;
        }
        ServerEntry SI = DataManager.instance.serverData;

        //RestClient.Request(new RequestHelper
        //{
        //    Uri = PostUrl, 
        //    Method = "POST",
        //    Timeout = 10,
        //    BodyString = "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}",
        //    Retries = 5
        //}).Then(response =>
        //{
        //    Log.Log("POST SUCCESS!");
        //}).Catch(err =>
        //{
        //    var error = err as RequestException;
        //    Log.Log("POST FAILED\n" + error);
        //});
        string postdata = "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}";
        Log.Log(postdata);
        RestClient.Post(PostUrl, "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}").Then(
            reponse =>
            {
                Log.Log("Posting Data");
            }, error =>
            {
                Log.Log("POST FAILED\n" + error);
            }
            );
    }
    
    public void PutData()
    {
        if (DataManager.instance.serverData == null)
        {
            return;
        }
        ServerEntry SI = DataManager.instance.serverData;

        //RestClient.Request(new RequestHelper
        //{
        //    Uri = PostUrl + "/" + DataManager.instance.DataReplacementValue,
        //    Method = "PUT",
        //    Timeout = 10,
        //    BodyString = "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}",
        //    Retries = 5
        //}).Then(response =>
        //{
        //    Log.Log("POST SUCCESS!");
        //}).Catch(err =>
        //{
        //    var error = err as RequestException;
        //    Log.Log("POST FAILED\n" + error);
        //});

        RestClient.Put(PostUrl + "/" + DataManager.instance.DataReplacementValue, "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}").Then(
            reponse =>
            {
                Log.Log("Putting Data");
            }, error =>
            {
                Log.Log("PUT Failed\n" + error);
            }
            );
    }
}


