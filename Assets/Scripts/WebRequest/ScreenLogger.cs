﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenLogger : MonoBehaviour
{
    [SerializeField]
    private Text UI_Logger;
    // Start is called before the first frame update
    void Start()
    {
        UI_Logger.text = "Log Started...";
    }


    public void Log(string Log)
    {
        UI_Logger.text += "\n" + Log;
    }

    public void ClearLog()
    {
        UI_Logger.text = "";
    }
}
