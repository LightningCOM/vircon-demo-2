﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ServerEntry
{
    public string name;
    public string type;
    public ServerData data;
}

[System.Serializable]
public class ServerData
{
    public float number1;
    public float number2;
    public float number3;
    public float number4;
    public Dictionary<string, string> pair;
}
