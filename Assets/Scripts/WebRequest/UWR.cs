﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class UWR : MonoBehaviour
{
    public string GetUrl;
    public string PostUrl;

    [SerializeField]
    private ScreenLogger Log;
    // Start is called before the first frame update
    #region GetDATA
    public void GetData()
    {
        Log.Log("Attempting Get!");
        StartCoroutine(GetRequest(GetUrl));
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();
            if (!webRequest.isNetworkError)
            {
                Debug.Log("Recieved: " + webRequest.downloadHandler.text);
                //Log.Log("Recieved: " + webRequest.downloadHandler.text);
                var res = JsonConvert.DeserializeObject<List<ServerEntry>>(webRequest.downloadHandler.text);
                foreach(var data in res)
                {
                    if(data.type == "test")
                    {
                        Log.Log("Name: " + data.name + "Type: " + data.type + " Value1: " + data.data.number1); ;
                    }
                }
            }
            else
            {
                Log.Log("Data Cannot be recieved");
            }
        }
    }
    #endregion GetDATA


    public void PostData()
    {
        StartCoroutine(PostRequest(PostUrl));
    }

    IEnumerator PostRequest(string uri)
    {
        ServerEntry SI = DataManager.instance.serverData;

        string Bodystring = "{ \"data\":" + JsonConvert.SerializeObject(SI) + "}";
        //WWWForm form = new WWWForm();
        //form.AddField("name", "dataTest");
        //form.AddField("type", "test");

        //DataType data = new DataType();
        //data.number1 = 50;
        //data.number2 = 60;
        //data.number3 = 40;
        //data.number4 = 80;

        //form.AddField("data", JsonConvert.SerializeObject(data));
        //Debug.Log(form.data);
        Debug.Log(Bodystring);

        using (UnityWebRequest www = UnityWebRequest.Post(uri, Bodystring))
        {
            yield return www.SendWebRequest();
            if (!www.isNetworkError)
            {
                Log.Log("Data Upload Success!!");
            }
            else
            {
                Log.Log("Data Upload Failure\n" + www.error);
            }
        }
    }

    public void PutData()
    {

    }
}
