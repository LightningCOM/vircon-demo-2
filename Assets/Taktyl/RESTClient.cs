﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Taktyl
{
    public class RESTClient : MonoBehaviour
    {
        public static IEnumerator Get(string uri, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Get(uri))
            {
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Post(string uri, string bodyString, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Put(uri, bodyString))
            {
                request.method = "POST";
                request.SetRequestHeader("Content-Type", "application/json");
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Post(string uri, WWWForm bodyForm, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Post(uri, bodyForm))
            {
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Post(string uri, Dictionary<string, string> bodyFields, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Post(uri, bodyFields))
            {
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Put(string uri, string bodyString, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Put(uri, bodyString))
            {
                request.SetRequestHeader("Content-Type", "application/json");
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Put(string uri, byte[] bodyData, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Put(uri, bodyData))
            {
                yield return request.SendWebRequest();

                response(request);
            }
        }

        public static IEnumerator Delete(string uri, Action<UnityWebRequest> response)
        {
            using (UnityWebRequest request = UnityWebRequest.Delete(uri))
            {
                yield return request.SendWebRequest();

                response(request);
            }
        }
    }
}
