﻿using SpaceHub.Conference;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public enum TeamType
{
    empty,
    team1,
    team2,
    team3,
    team4,
    team5,
    team6,
    team7,
    team8,
    team9,
    team10,
    team11,
    team12,
    team13,
    team14,
    team15,
    team16,
    team17,
    team18,
    team19,
    team20,
    team21

}

[System.Serializable]
public class teamInfo
{
    public TeamType teamType;
    public string teamPassword;
}
public  class TeamManager : MonoBehaviour
{
    public GameObject passwordCanvas;
    public InputField inputfieldPass;
    public Text statusText;

    string teamPassword;
    LoadSceneInteractable teamScene;

    public teamInfo[] teamInfo;
    private void Start()
    {
        passwordCanvas.SetActive(false);
    }
    public void ClosePassword()
    {
        inputfieldPass.text = "";
        statusText.text = "Enter Password..";
        teamPassword = null;
        teamScene = null;
        passwordCanvas.SetActive(false);
         
    }

    public void OnChangeText()
    {
      //  statusText.text = "Enter Password..";
    }


    public void LogIn()
    {
        if(inputfieldPass.text == null || inputfieldPass.text == "")
        {
            statusText.text = "Please Input Password!";
            return;
        }

        if (teamPassword == inputfieldPass.text)
        {
            // password success load the 
            statusText.text = "Correct!";
            //teamScene.LoadSceneWithPassword(); // load the scene!
        }
        else
        {
            // password failed
            statusText.text = "Wrong, Try Again!";
            inputfieldPass.text = "";
        }
    }

    public void ShowLogInPanel(TeamType teamType, LoadSceneInteractable loadSceneInteractable)
    {
        passwordCanvas.SetActive(true); 
        teamScene = loadSceneInteractable;

        for(int i = 0; i < teamInfo.Length; i++)
        {
            if(teamType == teamInfo[i].teamType)
            {
                // pass in the password!
                teamPassword = teamInfo[i].teamPassword;

            }
        }

    }
}
