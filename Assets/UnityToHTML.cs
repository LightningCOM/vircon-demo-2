﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using UnityEngine.Networking;
using System.Collections;

public class UnityToHTML : MonoBehaviour
{

#if UNITY_WEBGL && !UNITY_EDITOR

    [DllImport("__Internal")]
    private static extern void OpenModal(string str);

    [DllImport("__Internal")]
    private static extern void toggleLive(string url);

#endif

    private string url = "";

    public void Start()
    {
        // initial code
        StartCoroutine(GetUrl());
    }
    IEnumerator GetUrl()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://vircondev.taktylstudios.com/liveurl.txt");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            url = www.downloadHandler.text; 
            Debug.Log("youtube url: " + url);
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    public void SendToJS()
    { 
        Debug.Log("Sending Message to javascript : " + url);
#if UNITY_WEBGL && !UNITY_EDITOR

        toggleLive(url);

#endif
    }

    private void OnMouseDown()
    {
        Debug.Log("Debug: " + gameObject.name);
        SendToJS();
    }
}
