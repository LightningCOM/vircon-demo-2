﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Location
{
    public string sceneName;
    public string roomName;
    public string spawnPointID;
    public bool isAddressable;
}
public class FastTravel : MonoBehaviour
{
    public static FastTravel instance;
    [SerializeField]
    private List<Location> scenes;
    private void Awake()
    {
        if(instance != this)
            instance = this;
    }

    [HideInInspector]
    public List<Location> locations { get { return scenes;} }
}
