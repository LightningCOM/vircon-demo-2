﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SpaceHub.Conference
{
    public class LocationData : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI sceneName;

        [SerializeField]
        private LocationList sceneList;

        private string scene;
        private string spawnPoint;
        private bool isAdressable;

        public void SetData(string name, string scene_id, string spawn, bool addressable)
        {
            sceneName.text = name;
            scene = scene_id;
            spawnPoint = spawn;
            isAdressable = addressable;
            gameObject.SetActive(true);
        }

        public void LoadSelection()
        {
            sceneList.ToggleMenu();
            ConferenceRoomManager.LoadRoom(scene, spawnPoint, null, isAdressable);
        }

    }

}
