﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceHub.Conference
{
    public class LocationList : MonoBehaviour
    {
        [SerializeField]
        private LocationData locationObject;

        [SerializeField]
        private Transform parentObject;

        [SerializeField]
        private Animator animation;

        bool isOpen;
        private void Start()
        {
            isOpen = false;
            foreach (Location destination in FastTravel.instance.locations)
            {
                locationObject = Instantiate(locationObject, parentObject);
                locationObject.SetData(destination.roomName, destination.sceneName, destination.spawnPointID, destination.isAddressable);
                //locationObject.gameObject.SetActive(true);
            }
        }

        public void ToggleMenu()
        {
            if (isOpen)
            {
                //CloseAnimation
                animation.SetTrigger("Close");
                isOpen = false;
            }
            else if(!isOpen)
            {
                animation.SetTrigger("Open");
                parentObject.gameObject.SetActive(true);
                isOpen = true;
            }
        }
    }

}
