﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
//using UnityEngine.ScreenCaptureModule;
using TMPro;
using System;

public class PhotoMode : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void DownloadFile(byte[] array, int byteLength, string fileName);

    [SerializeField]
    private GameObject UI, CompanyLogo;

    [SerializeField]
    private TextMeshProUGUI TextDate;

    DateTime date;
    private void Start()
    {
        date = DateTime.Now;
    }
    public void Screenshot()
    {
        StartCoroutine(Capture());
    }

    IEnumerator Capture()
    {
        UI.SetActive(false);
        CompanyLogo.SetActive(true);
        TextDate.text = string.Format("{0}/{1}/{2}", date.Month, date.Day, date.Year);
        Debug.Log("PHOTO TAKEN");
        yield return new WaitForEndOfFrame();
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture(2);
        byte[] textureBytes = texture.EncodeToJPG();
        DownloadFile(textureBytes, textureBytes.Length, "screenshot.jpg");
        Destroy(texture);

        Debug.Log("PHOTO DONE!");
        UI.SetActive(true);
        CompanyLogo.SetActive(false);
        yield return null;
    }
}
