﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class InitializeVP : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoPlayer;

    Shader shader;
    Material material;
    [SerializeField]
    private RenderTexture renderTexture;
    new Renderer renderer;
    void Start()
    {
        videoPlayer = GetComponentInChildren<VideoPlayer>();
        renderer = GetComponentInChildren<Renderer>();
        shader = Shader.Find("Universal Render Pipeline/Unlit");
        material = new Material(shader);

        renderTexture = new RenderTexture(1280, 720, 24)
        {
            width = 1280,
            height = 720,
            dimension = UnityEngine.Rendering.TextureDimension.Tex2D,
            graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm,
            useDynamicScale = true
        };
        bool didRenderTexCreate = renderTexture.Create();

        material.SetTexture("_BaseMap", renderTexture);
        renderer.material = material;
        renderer.rendererPriority = 1;
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
        videoPlayer.targetTexture = renderTexture;

    }
}
