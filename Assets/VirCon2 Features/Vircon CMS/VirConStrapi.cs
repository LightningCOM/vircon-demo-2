﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using Newtonsoft.Json;

[System.Serializable]
public class StrapiMediaData
{
    public string name;
    public string type;
    public string url;
}

public enum StrapiMediaType
{
    image,
    video
}
public class VirConStrapi : MonoBehaviour
{
    public static VirConStrapi instance;
    [SerializeField]
    private string GetUrl;
    [SerializeField]
    private StrapiMediaType mediaType;

    [Tooltip("For both Images and Videos")]
    [SerializeField]
    private bool isGlobal;

    [SerializeField]
    private List<StrapiMediaData> media;

    [Header("For Development under local strapi")]
    [SerializeField]
    private bool isLocal;
    private string baseUrl = "http://localhost:1337";
    bool dataReady;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        dataReady = false;
        media = new List<StrapiMediaData>();
        GetData();
    }

    private void GetData()
    {
        RestClient.Get(GetUrl).Then(response =>
        {
            var res = JsonConvert.DeserializeObject<List<StrapiMediaData>>(response.Text);
            foreach(var data in res)
            {
                if (isLocal)
                    data.url = baseUrl + data.url;
                if (isGlobal)
                    media.Add(data);
                else if(data.type == mediaType.ToString())
                    media.Add(data);
            }
            dataReady = true;
        }).Catch(error =>
        {
            Debug.Log("Error on Gathering Server Data");
        });
    }

    public List<StrapiMediaData> data { get { return media; } }
    public bool isReady { get { return dataReady; } }
}
