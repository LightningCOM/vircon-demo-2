﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using Newtonsoft.Json;

[System.Serializable]
public class StrapiMultiMediaData
{
    public string name;
    public List<string> urls;
}
public class VirConStrapi_Multimedia : MonoBehaviour
{
    public static VirConStrapi_Multimedia instance;
    [SerializeField]
    private string GetUrl;

    [SerializeField]
    private List<StrapiMultiMediaData> media;

    [Header("For Development under local strapi")]
    [SerializeField]
    private bool isLocal;
    private string baseUrl = "http://localhost:1337";
    bool dataReady;
    private void Awake()
    {
        instance = this;
        dataReady = false;
    }
    private void Start()
    {
        media = new List<StrapiMultiMediaData>();
        GetData();
    }

    private void GetData()
    {
        RestClient.Get(GetUrl).Then(response =>
        {
            var res = JsonConvert.DeserializeObject<List<StrapiMultiMediaData>>(response.Text);
            foreach (var data in res)
            {
                if (isLocal)
                {
                    for(int i = 0; i < data.urls.Count; i++)
                    {
                        data.urls[i] = baseUrl + data.urls[i];
                    }
                }
                media.Add(data);
            }
            dataReady = true;
        }).Catch(error =>
        {
            Debug.Log("Error on Gathering Server Data");
        });
    }
}
