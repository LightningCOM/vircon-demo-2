﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Proyecto26;
using Newtonsoft.Json;
using UnityEngine.Networking;

public class VirCon_CMS_Image : MonoBehaviour
{
    [SerializeField]
    private Image spriteImage;

    string name;
    // Start is called before the first frame update
    void Start()
    {
        name = gameObject.name;
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        yield return new WaitUntil(() => VirConStrapi.instance.isReady);
        foreach(StrapiMediaData SMD in VirConStrapi.instance.data)
        {
            if(SMD.name == name)
            {
                GetImage(SMD.url);
                break;
            }
        }
    }
    void GetImage(string uri)
    {
        RestClient.Get(new RequestHelper
        {
            Uri = uri,
            DownloadHandler = new DownloadHandlerTexture()
        }).Then(response =>
        {
            var DownloadedTexture = ((DownloadHandlerTexture)response.Request.downloadHandler).texture;
            spriteImage.sprite = Sprite.Create(DownloadedTexture, new Rect(0, 0, DownloadedTexture.width, DownloadedTexture.height), Vector2.zero);
            spriteImage.sprite.name = "1";
        }).Catch(error =>
        {
            Debug.Log("Error at: " + name + " getting sprite from " + uri + "\n" + error);
        });
    } 
}
