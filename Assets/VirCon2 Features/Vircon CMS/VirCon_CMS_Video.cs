﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Proyecto26;
using Newtonsoft.Json;

public class VirCon_CMS_Video : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoPlayer;

    string name;

    private void Start()
    {
        name = gameObject.name;
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        yield return new WaitUntil(() => VirConStrapi.instance.isReady);
        foreach (StrapiMediaData SMD in VirConStrapi.instance.data)
        {
            if(SMD.name == name)
            {
                videoPlayer.url = SMD.url;
                break;
            }
        }
    }
}
