﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceHub.Groups
{

    public class VoiceChannel : MonoBehaviour
    {
        VoiceManager m_VoiceManager;
        string m_CurrentRoom;
        // Start is called before the first frame update
        void Start()
        {
            m_CurrentRoom = SceneManager.GetActiveScene().name;
            Debug.Log("Awake:" + SceneManager.GetActiveScene().name);

            m_VoiceManager = GameObject.FindGameObjectWithTag("VoiceChat").GetComponent<VoiceManager>();
            m_VoiceManager.JoinRoom(m_CurrentRoom);
        }

        private void OnDestroy()
        {
            m_VoiceManager.LeaveRoom();
        }
    }
}