﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCamDisplay : MonoBehaviour
{

    private WebCamTexture backCam;
    private bool camAvailable;
    private bool isFrontCam;
    private Texture defaultBackground;
    private WebCamDevice[] devices;
    public MeshRenderer meshRender;
    private Camera renderCam;
    // Start is called before the first frame update
    private void Start()
    {
        renderCam = transform.parent.GetComponent<Camera>();
        float distance = Vector3.Distance(renderCam.transform.position, transform.position);
        float height = 2.0f * Mathf.Tan(0.5f * renderCam.fieldOfView * Mathf.Deg2Rad) * distance;
        float width = height * Screen.width / Screen.height;
        transform.localScale = new Vector3(width / 10f, 1.0f, height / 10f);

        defaultBackground = meshRender.material.mainTexture;
        devices = WebCamTexture.devices;

        if(devices.Length == 0)
        {
            Debug.Log("No camera detected!");
            camAvailable = false;
            return;
        }

        for(int i = 0; i < devices.Length; i++)
        {

            Debug.Log("Camera devices name: " + i + " " + devices[i].name);
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
                isFrontCam = false;
            }
        }

        if(backCam == null)
        {
            Debug.Log("Unable to find back camera");
            backCam = new WebCamTexture(devices[0].name, Screen.width, Screen.height);
            isFrontCam = true;
            Debug.Log("Use available cam " + devices[0].name); 
        }

        backCam.Play();
        meshRender.material.mainTexture = backCam; 
        camAvailable = true;
    } 

    public void SwitchCam()
    {

        if(devices.Length >= 2)
        { 
            backCam.Stop();

            if (isFrontCam)
            {
                // switch to back cam 
                backCam = new WebCamTexture(devices[1].name, Screen.width, Screen.height);
                isFrontCam = false;
            }
            else
            {
                // switch to front cam
                backCam = new WebCamTexture(devices[0].name, Screen.width, Screen.height);
                isFrontCam = true;
            }

            backCam.Play();
            meshRender.material.mainTexture = backCam;
        }
        else
        {
            Debug.Log("There is only 1 cam cannot switch!");
        }

    }
}
